#### Disclaimer:  
Bullet Points do not describe everything.  
Points describe content that is important to moderator,  
THERE WILL BE MORE INFORMATION THAN POINTS DESCRIBE.  


# Articles
* [Harvard Study Confirms Fluoride Reduces Children's IQ](https://www.huffpost.com/entry/fluoride_b_2479833)
  * Harvard study.
  * Metastudy of chinese IQ research
* [Water Fluoridation: A Critical Review of the Physiological Effects of Ingested Fluoride as a Public Health Intervention](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3956646/)
  * Review of human health effects from fluoride
* [Developmental Fluoride Neurotoxicity: A Systematic Review and Meta-Analysis](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3491930/)
  * Study of fluoride on the mental development of children.
* [Toxicity of fluoride in liver of Albino rat and Mitigation after adopting artificial (Vitamin C and D) and natural (Aloe vera) food supplementations.](http://www.ijoart.org/docs/Toxicity-of-fluoride-in-liver-of-Albino-rat-and-Mitigation-after-adopting-artificial-Vitamin-C-and-D.pdf)
  * Effects of fluoride on liver (fluorosis)
  * PDF
* [RECOVERY FROM FLUORIDE AND ALUMINIUM INDUCED FREE RADICAL LIVER TOXICITY IN MICE](http://www.fluorideresearch.org/374/files/374257-263.pdf)
  * Interest points in title
* [MAJOR FINDINGS](https://health.gov/environment/ReviewofFluoride/MAJfind.htm)
  * Assessments of health benefits and risks from Fluoride