#### Disclaimer:  
Bullet Points do not describe everything.  
Points describe content that is important to moderator,  
THERE WILL BE MORE INFORMATION THAN POINTS DESCRIBE.  


# Videos
* [Jeff Volek - The Many Facets of Keto-Adaptation: Health, Performance, and Beyond](https://www.youtube.com/watch?v=GC1vMBRFiwE)
  * Research and tests on athletes 
  * Shows higher performance and stamina on ketones
* [Prof. Jeff Volek - 'Nutrition for Optimising Athletic Performance'](https://www.youtube.com/watch?v=tQbgdRoAfOo)
  * Research detailing improvement of athletic performance on Ketones
* [A List of Scientific Studies and Research on Low Carb High Fat Diet](https://www.ketovale.com/low-carb-keto-diet-studies-list/)
  * Research detailing improvement of mental performance on Ketones
* [A case study. APOE ε4, the door to insulin-resistant dyslipidemia and brain fog?](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6423699/)
  * Research detailing improvement of mental performance on Ketones
* [Effects of beta-hydroxybutyrate on cognition in memory-impaired adults. - PubMed](https://www.ncbi.nlm.nih.gov/pubmed/15123336)
  * Research detailing improvement of mental performance on Ketones
* [Novel ketone diet enhances physical and cognitive performance. - PubMed](https://www.ncbi.nlm.nih.gov/pubmed/27528626)
  * Research detailing improvement of mental performance on Ketones


# Articles
* [The Emerging Science on Fat Adaptation](https://ultrarunning.com/features/health-and-nutrition/the-emerging-science-on-fat-adaptation/)
  * Research detailing improvement of athletes' performance on Ketones

