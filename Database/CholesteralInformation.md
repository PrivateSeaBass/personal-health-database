#### Disclaimer:  
Bullet Points do not describe everything.  
Points describe content that is important to moderator,  
THERE WILL BE MORE INFORMATION THAN POINTS DESCRIBE.  


# Videos
* Everything You Know About Cholesterol Is Wrong [Part 1](https://www.youtube.com/watch?v=_dltdsxeoQk) [Part 2](https://www.youtube.com/watch?v=DCY5Bzs11W4) 
  * Two doctors telling of their experience with Saturated Fats and cholesterol having favorable affects for their patients.
* [Dr. Rhonda Patrick Explains the Cause of Heart Disease](https://www.youtube.com/watch?v=uF0FhGzroS4)
  * Discussion on cholesterol
  * What cholesterol does
  * What cholesterol does not do
* [CHOLESTEROL MYTH: Why Cholesterol Is Not the Enemy](https://www.youtube.com/watch?v=YLXKpVqDr-o)
  * Damage shown to happen when people reduce cholesterol
  * Poor results in avoiding saturated fats
* The Cholesterol Conundrum - [Root Cause Solution](https://www.youtube.com/watch?v=fuj6nxCDBZ0) and [Putting LDL Particle Count into Context](https://www.youtube.com/watch?v=hFkrGIYIM74)
  * Describes types of cholesterol
  * Describes what each cholesterol does
  * Describes the life cycle of each cholesterol
  * Describes what happens when cholesterol goes rogue
  * A description of how and why LDL goes through cell walls
  * How and why LDL goes into cells
  * How and why LDL goes through cells
  * How and why LDL bypasses cells
  * Contains studies on when and how LDL becomes damaging