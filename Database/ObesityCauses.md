#### Disclaimer:  
Bullet Points do not describe everything.  
Points describe content that is important to moderator,  
THERE WILL BE MORE INFORMATION THAN POINTS DESCRIBE.  


# Videos
* [Dr. Michael Eades - 'A New Hypothesis Of Obesity'](https://www.youtube.com/watch?v=pIRurLnQ8oo)
  * First half is introduction
  * Second half is effets on Midocondria from saturated fats vs polyunsaturated fats in Midocondria
* [How to maximize fat burning - Diet Doctor (Dr. Jason Fung)](https://www.dietdoctor.com/how-to-maximize-fat-burning)
  * Effects of insulin on human metabolism
  * How to access fat for energy
  * One solution to Obesity (alternatively, what people with Obesity don't do/know)
  